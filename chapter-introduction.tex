%======================================================================
\chapter{Introduction}
%======================================================================
\section{Background}
Recent years have witnessed exponential growth in the adoption of cloud services. From SMEs to tech giants and governments, cloud service has become an integral part of supporting organizations' daily operations. Among all the cloud service providers (CSPs), Amazon Web Service (AWS), Google Cloud Platform (GCP), and Microsoft Azure (Azure) are some of the biggest players in the cloud computing market.

The idea of multi-cloud is associated with utilizing services from multiple CSPs simultaneously for a system or an application. It is estimated that more than 85\% of the organizations have shifted their operations in multi-cloud environments \cite{cloud-orchestra}. Multi-cloud paradigm offers extra benefits to users and enterprises such as mitigating the risk of vendor lock-in, optimizing performances and costs, ensuring high availability, ensuring data sovereignty, and offering services that are particular to one CSP but not offered elsewhere \cite{10.1145/3069383.3069387}\cite{6740239}\cite{10.1145/2462326.2462328}\cite{10.1007/978-3-030-44081-7_12}. 

Due to  these reasons, given a distributed application, an enterprise may want some of its components on one cloud and some other components on other clouds as a result of business decisions such as maximizing the benefits of services provided by different clouds. In this scenario, provisioning connectivity to interconnect these clouds, which requires expert knowledge of multiple clouds involved, becomes an essential part of managing and maintaining such distributed applications. Also, under certain scenarios, enterprises may want to migrate applications from one cloud to another cloud due to economical or compliance reasons. One of the biggest challenges of managing multi-cloud applications or cloud migrations is to achieve consistent security objectives across all clouds involved, but this is often difficult to achieve because the security features offered by different CSPs differ in their syntax and semantics.

This research work aims at exploring the provision of connectivity with security attributes in the multi-cloud setting. Specifically, in chapter \ref{chapter-security-attributes}, we formalize the syntax and semantics of security features offered by each CSP and try to address the following questions:
\begin{enumerate}
\item Understand the expressive power of the portion of the security configuration that pertains to connectivity, i.e., given multiple CSPs, does one CSP's security language syntax offer more than that of other CSPs'?
\item Given a security configuration for one CSP, does there exist an equivalent security configuration in another CSP? If not, what is the tightest/best security configuration in another CSP?
\item What is an efficient algorithm to translate one CSP's security configuration to the best security configuration in another CSP?
\end{enumerate}

In chapter \ref{chapter-provisioning-connectivity}, we describe the design and development of graph-based software that helps the management of multi-cloud applications and the provision of connectivity in multi-cloud environments.

%======================================================================
\section{Definition}
%======================================================================
\newtheorem{definition}{Definition}

\begin{definition}\label{Cloud}
A cloud is one of the cloud service providers (CSPs), namely Amazon Web Service (AWS), Google Cloud Platform (GCP), or Microsoft Azure (Azure).
\end{definition}

\begin{definition}\label{Subnet}
A subnet is a segmented piece of a larger network, i.e., a network inside a larger network.
\end{definition}

\begin{definition}\label{Private Subnet}
A private network is a computer network that uses a private address space of IP addresses.
\end{definition}
A private address space is a subset of the following address ranges:
\begin{itemize}
    \item 10.0.0.0 - 10.255.255.255
    \item 172.16.0.0 - 172.31.255.255
    \item 192.168.0.0 - 192.168.255.255
\end{itemize}
Resources such as computers inside a private network will be assigned private IP addresses.

\begin{definition}\label{Private IP Address}
A private IP address is an IP address that is within the private address space.
\end{definition}

\begin{definition}\label{CIDR Block}
A CIDR (Classless Inter-Domain Routing) Block defines a range of consecutive IPv4 addresses. A CIDR block is of the form a.b.c.d/n such that a, b, c, d each is a number of up to three digits, 0-255, and n is a number from 0-32 which represents the network mask.
\end{definition}

\begin{definition}\label{Non-overlapping CIDR Blocks}
Two CIDR blocks, a.b.c.d/n and e.f.g.h/m are Non-overlapping if and only if the conjunction of two sets of IP addresses defined by the two CIDR blocks is an empty set.
\end{definition}

\begin{definition}\label{VPC}
A Virtual Private Cloud (VPC) is a private network that is hosted on a cloud. Formally, a VPC is defined as a 2-tuple:

\begin{center}
    $VPC = \langle cloud, cidrBlock \rangle$
\end{center}
\end{definition}

A VPC resembles a private network whose CIDR block defines the private address space within the VPC. A VPC may be partitioned into smaller non-overlapping subnets.

\begin{definition}\label{Virtual Machine (VM)}
A Virtual Machine (VM) is a computer resource that is defined by a 3-tuple, namely 

$\langle \texttt{ipAddress, subnet, securityAttribute?} \rangle$, where $ipAddress \in subnet$ and ? means optional.
\end{definition}

A VM may be \emph{deployed} in a cloud. What this means is that an instance of a computing resource is reserved within a cloud. A deployed VM can also be attached to a subnet of a VPC and therefore, a VM that is attached to a VPC can have a private IP address that is within the VPC CIDR block. A VM is sometimes referred as an \emph{instance} in the cloud.

\begin{definition}\label{IP Packet}
An IP Packet, $pk_{ip}$, is a 7-tuple:
\begin{center}
$(ip_{src}, ip_{dest}, protocol, port_{src}?, port_{dest}?, type_{icmp}?, code_{icmp}?)$
\end{center}
where ? signifies optional component.
\end{definition}

\begin{definition}\label{VPC Connectivity}
An instance of connectivity between two VPCs is a function of exactly two VPCs, which may not be distinct from one another. We say that there exists an instance of connectivity between two VPCs if and only if, without additional security attributes, any VMs in one VPC is able to send and receive ip packet to and from any VMs in the other VPC.
\end{definition}

\begin{definition}\label{VM Connectivity}
An instance of connectivity between two VMs is a function of exactly two VMs, which may not be distinct from one another. There exists an instance of connectivity between two VMs if and only if one VM is able to send and receive ip packet to and from the other VM via the private IP address of the other VM.
\end{definition}

\begin{definition}\label{Stateful}
A stateful security configuration is one such that if an ip packet $pk_{ip}$ is allowed or denied by the stateful security configuration in one direction, then the packet $pk_{res}$ generated in response to $pk_{ip}$ is allowed in the opposite direction regardless of the security configuration in the opposite direction.
\end{definition}

\begin{definition}\label{Stateless}
A stateless security configuration is one such that the packets $pk_{res}$ generated in response to an allowed ip packet $pk_{ip}$ in one direction are subject to the security configuration for $pk_{res}$ in the opposite direction.
\end{definition}

\section{Related Work}
Over the last decade, many studies explored the multi-cloud paradigm and proposed applications based on multiple clouds, especially for the ones that pertain to using multiple clouds to preserve network or data security and privacy. For example, D\textsc{ep}S\textsc{ky} is a multi-cloud storage system that addresses the risks that are otherwise common in single-cloud storage, namely, the loss of availability of data, data loss, data corruption, and the loss of privacy \cite{10.1145/2535929}. It achieves these by combining Byzantine quorum system protocols, cryptography, secretes sharing, erasure code, and the diverse nature of multi-cloud together \cite{10.1145/2535929}. In addition, it reduces the coupling between users and CSPs so that vendor lock-in can be eliminated. To prevent the loss of data availability, DEPSKY replicates data and stores them in multiple clouds such that in the event of a subset of CSPs' service outage, the data remains accessible. DEPSKY also uses erasure codes to reduce the amount of replication needed, thus reducing storage costs. By using a set of efficient Byzantine quorum system protocols, DEPSKY offers protection against data loss and the corruption of data \cite{10.1145/2535929}.  

Stefanov \& Shi [2013] proposed a multi-cloud protocol named Multi-Cloud Oblivious Storage (MCOS) which aimed at leveraging multiple clouds to protect data confidentiality and anonymity in the public cloud computing setting \cite{10.1145/2508859.2516673}. This protocol utilizes 2 non-colluding clouds, but it can be further expanded to include more CSPs \cite{10.1145/2508859.2516673}. In the 2-cloud oblivious storage implementation, the client shares a symmetric secret key with each cloud which will be used for encryption and decryption. The client initializes the storage by partitioning the original Oblivious Random Access Memory (ORAM) of size $N$ into $O(\sqrt{N})$ partitions that are themselves ORAMs \cite{10.1145/2508859.2516673}. Each partition contains $\log{N}$ levels in a way that is similar to a binary tree data structure. Each level can contain data blocks or dummy (empty) blocks, and which level resides in which cloud can change as partitions are shuffled between clouds during a read or a write operation later on. Each data block is then placed randomly into a partition and assigned a random offset from the top level, i.e., the largest level, of that partition. To keep track the exact location of each data block, the client stores locally a position map that contains a tuple (\textit{partition}, \textit{level}, \textit{offset}) for each data block. The tuple reveals information about the data block's exact location in a particular partition \cite{10.1145/2508859.2516673}. 

Another multi-cloud paradigm, Prio, proposed by Gibbs and Boneh [2017], is a privacy-preserving system for collecting aggregate statistics \cite{201553}. Prio is proven to be robust, scalable, and can be deployed in the multi-cloud setting. Prio achieves the goal of \textit{correctness}, \textit{privacy}, \textit{robustness}, and \textit{efficiency}. Using Prio, if all clouds are honest, then the clouds collectively compute the correct aggregate statistics (\textit{correctness}). If at least one cloud is honest, then Prio protects users' \textit{privacy}. Malformed data from malicious clients have limited impact only (\textit{robustness}). Furthermore, Prio is able to handle a high volume of user data (\textit{efficiency}) \cite{201553}.  

We notice that these studies mainly focused on leveraging the multi-cloud paradigm to enforce data confidentiality and preserve data privacy, while little attention was directed to achieving multi-cloud connectivity in general. Furthermore, these studies did not consider cloud-managed security solutions either.

Recently, Yeganeh \textit{et al.} [2020] conducted a comprehensive study on the characterization of multi-cloud connectivity involving major public cloud vendors. In this study, they compare three major multi-cloud connectivity paradigm, namely (1) transit provider-based best-effort public Internet (BEP), (2) third-party provider-based private (TPP) connectivity, and (3) CP-based private (CPP) connectivity \cite{10.1007/978-3-030-44081-7_12}. The study found that CPP-based multi-cloud connectivity has low network latency and is more stable compared to BEP and TPP-based multi-cloud connectivity. It also showed that CPP-based connectivity has higher throughput and less variation \cite{10.1007/978-3-030-44081-7_12}. These findings encouraged this research as it backed up the fact that cloud-managed multi-cloud connectivity provisioning, which will be covered in chapter \ref{chapter-security-attributes}, is indeed a feasible solution and exhibits certain benefits compared to other multi-cloud connectivity provisioning paradigms.

% \begin{definition}\label{Security Approach}
% A security approach is one of the preventative and detective.
% \end{definition}
% Common security measures that adopts the \emph{preventative} approach include:
% Access Control Lists (ACLs), Encryption, and Firewalls

% Common seucirty measures that adopts the \emph{detective} approach include:
% Flow Logs, Traffic Monitoring

% \begin{definition}\label{VM Connectivity}
% A security attribute is a 2-tuple, (str Approach, bool Implemented), where the first element, i.e., Approach, is a string specifying the security approach, and the second element, i.e., Implemented, is a boolean value indicating whether the specified approach has been implemented by the instance of connectivity.
% \end{definition}
% A security attribute can be attached to an instance of connectivity between two VPCs or an instance of connectivity between two VMs.

